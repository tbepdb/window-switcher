SHELL = bash
UUID = window-switcher@tbepdb

BUILD_DIR ?= .
RAW_PATH = "$(BUILD_DIR)/$(UUID).shell-extension.zip"
BUNDLE_PATH = "$(BUILD_DIR)/$(UUID).zip"

.PHONY: build package check release install uninstall clean

build: clean
	$(MAKE) package
	@mv -v $(RAW_PATH) $(BUNDLE_PATH)
package:
	@echo "Packing files..."
	gnome-extensions pack --force \
	--extra-source=schemas \
	--extra-source=extension.js \
	--extra-source=metadata.json \
	--extra-source=prefs.js \
	-o ./
check:
	@if [[ ! -f $(BUNDLE_PATH) ]]; then \
	  echo -e "\nWARNING! Extension zip couldn't be found"; exit 1; \
	elif [[ "$$(stat -c %s $(BUNDLE_PATH))" -gt 4096000 ]]; then \
	  echo -e "\nWARNING! The extension is too big to be uploaded to the extensions website, keep it smaller than 4096 KB"; exit 1; \
	fi
install:
	@if [[ ! -f $(BUNDLE_PATH) ]]; then \
	  $(MAKE) build; \
	fi
	gnome-extensions install $(BUNDLE_PATH) --force
uninstall:
	gnome-extensions uninstall "$(UUID)"
clean:
	@rm -rfv "$(UUID).zip"
	@rm -rfv extension/ui/*.ui~ extension/ui/*.ui#